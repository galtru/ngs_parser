import urllib.request
import urllib.parse
import bs4
import re


class NgsFlatLinksParser:

    def __init__(self, first_url):
        self.first_url = first_url

    def get_last_page(self):
        html_page = urllib.request.urlopen(self.first_url)
        soup = bs4.BeautifulSoup(html_page, 'lxml')
        paginator_div = soup.findAll('div', attrs={'class': 'navigator'})
        paginator_links = paginator_div[0].findAll('a')
        last_page = round(
            int(re.search(';page=(.*)">', str(paginator_links[-1])).group(1)))
        return last_page

    def get_flat_links(self):
        last_page = self.get_last_page()
        flat_links = []
        html_class = 'last-col'
        for i in range(1, last_page + 1):
            if i == 1:
                url = self.first_url
            else:
                url = self.first_url + '&page=' + str(i)
            html_page = urllib.request.urlopen(url)
            soup = bs4.BeautifulSoup(html_page, 'lxml')
            data = soup.findAll('td', attrs={'class': html_class})
            for div in data:
                links = div.findAll('a')
                if links:
                    link = links[0]['href']
                    flat_links.append(link)
        return set(flat_links)
