import unittest
import random
import re
from ngs_parser.ngs_flat_parser import NgsFlatParser
from datetime import datetime


class TestNgsParser(unittest.TestCase):

    def setUp(self):
        flat_links_list = [
            'http://homes.ngs.ru/view/222845483/',
            'http://homes.ngs.ru/view/220721753/',
            'http://homes.ngs.ru/view/217876943/',
            'http://homes.ngs.ru/view/207817353/',
            'http://homes.ngs.ru/view/222845483/'
        ]
        flat_link = random.choice(flat_links_list)
        self.ngs = NgsFlatParser(flat_link)
        self.oldest_building_age = 1895
        self.max_rooms = 10
        self.max_price = 50000
        self.min_price = 500
        self.max_floor = 26

    def tearDown(self):
        self.ngs.close_browser()

    def test_get_rooms_has_valid_range(self):
        rooms = self.ngs.get_rooms()
        self.assertTrue(0 < rooms < self.max_rooms)

    def test_get_flat_square_has_total_sq_key(self):
        square_dict = self.ngs.get_square_dict()
        self.assertTrue(square_dict['total_sq'])

    def test_get_flat_square_all_sq_is_float(self):
        square_dict = self.ngs.get_square_dict()
        [self.assertTrue(isinstance(value, float)) for value in square_dict.values()]

    def test_get_price_contains_rub(self):
        price = self.ngs.get_price()
        self.assertTrue(re.findall('руб.', price))

    def test_format_price_has_valid_range(self):
        price = self.ngs.format_price()
        self.assertTrue(self.min_price < price < self.max_price)

    def test_get_street_return_valid_street(self):
        street = self.ngs.get_street('Тестовый текст ул. Некрасова, 45б')
        self.assertTrue('Некрасова' == street)

    def test_get_house_return_valid_house(self):
        house = self.ngs.get_house('Тестовый текст д д. 19/1в к2')
        self.assertTrue(house == '19/1в к2')

    def test_address_dict_has_address_and_house_keys(self):
        address = self.ngs.get_address_dict()
        self.assertTrue(address['street'])
        self.assertTrue(address['house'])

    def test_get_id_has_9_digits_length(self):
        flat_id = self.ngs.get_id()
        self.assertEqual(len(str(flat_id)), 9)

    def test_get_id_first_number_is_two(self):
        flat_id = self.ngs.get_id()
        self.assertEqual(str(flat_id)[0], '2')

    def test_get_detail_info_dict_contains_floors(self):
        detail_info_dict = self.ngs.get_detail_info_dict()
        self.assertTrue(detail_info_dict['Этаж'])
        self.assertTrue(detail_info_dict['Этажность'])

    def test_get_detail_info_dict_has_year_of_construction(self):
        detail_info_dict = self.ngs.get_detail_info_dict()
        self.assertTrue(detail_info_dict['Год постройки'])

    def test_format_detail_info_dict_has_floors_as_int_value(self):
        detail_info_dict = self.ngs.format_detail_info_dict()
        self.assertTrue(isinstance(detail_info_dict['Этаж'], int))
        self.assertTrue(isinstance(detail_info_dict['Этажность'], int))

    def test_format_detail_info_dict_floors_is_positive_int_to_26(self):
        detail_info_dict = self.ngs.format_detail_info_dict()
        self.assertTrue(0 < detail_info_dict['Этаж'] <= self.max_floor)
        self.assertTrue(0 < detail_info_dict['Этажность'] <= self.max_floor)

    def test_format_detail_info_dict_year_of_construction_less_or_equal_current_year(self):
        detail_info_dict = self.ngs.format_detail_info_dict()
        year = datetime.now().year
        self.assertLessEqual(detail_info_dict['Год постройки'], year)

    def test_format_detail_info_dict_year_of_construction_more_or_equal_oldest_house(self):
        detail_info_dict = self.ngs.format_detail_info_dict()
        self.assertGreaterEqual(detail_info_dict['Год постройки'], self.oldest_building_age)

    def test_get_commentory_doesnt_contain_word_commentary(self):
        commentory = self.ngs.get_commentary()
        self.assertFalse('Комментарий' in commentory)

    def test_get_owner_name_has_len_more_or_equal_2_letters(self):
        owner_name = self.ngs.get_owner_name()
        self.assertGreaterEqual(len(owner_name), 2)

    def test_get_owner_phones_list_start_with_7(self):
        phones = self.ngs.get_owner_phones_list()
        [self.assertEqual(phone[0], '7') for phone in phones]

    def test_get_owner_phones_len_11(self):
        phones = self.ngs.get_owner_phones_list()
        [self.assertEqual(len(phone), 11) for phone in phones]

    def test_get_owner_dict_contains_name_as_string(self):
        owner_dict = self.ngs.get_owner_dict()
        self.assertTrue(isinstance(owner_dict['name'], str))

    def test_get_owner_dict_contains_phones_as_list(self):
        owner_dict = self.ngs.get_owner_dict()
        self.assertTrue(isinstance(owner_dict['phones'], list))

    def test_create_flat_dict_contains_all_keys(self):
        flat_dict = self.ngs.create_flat_dict()
        self.assertTrue(flat_dict['id'])
        self.assertTrue(flat_dict['rooms'])
        self.assertTrue(flat_dict['square'])
        self.assertTrue(flat_dict['details'])
        self.assertTrue(flat_dict['owner'])
        self.assertTrue(flat_dict['commentary'])
        self.assertTrue(flat_dict['price'])
        self.assertTrue(flat_dict['address'])


class TestNotExistingPage(unittest.TestCase):

    def setUp(self):
        link = 'http://homes.ngs.ru/view/1/'
        self.ngs = NgsFlatParser(link)

    def tearDown(self):
        self.ngs.close_browser()

    def test_check_page_exist_false_when_its_not(self):
        is_exist = self.ngs.check_page_exist()
        self.assertFalse(is_exist)


class TestIsArchivedAd(unittest.TestCase):

    def setUp(self):
        link = 'http://homes.ngs.ru/view/208250563/'
        self.ngs = NgsFlatParser(link)

    def tearDown(self):
        self.ngs.close_browser()

    def test_check_page_in_archive_is_true_when_ad_in_archive(self):
        is_archived = self.ngs.check_page_in_archive()
        self.assertTrue(is_archived)


if __name__ == '__main__':
    unittest.main()
