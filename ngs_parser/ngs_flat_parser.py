import re
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException


class NgsFlatParser:

    def __init__(self, flat_link):
        chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.managed_default_content_settings.images": 2}
        chrome_options.add_experimental_option("prefs", prefs)
        self.browser = webdriver.Chrome(
            'c:/users/galtr/telescope_x/webdriver/chromedriver.exe',
            chrome_options=chrome_options)
        self.browser.get(flat_link)
        self.flat_link = flat_link

    def check_page_in_archive(self):
        head_line = self.browser.find_element_by_class_name('card__header').text
        is_archive = re.findall('в архиве', head_line)
        if is_archive:
            return True
        else:
            return False

    def check_page_exist(self):
        try:
            self.browser.find_element_by_class_name('card__cost')
            return True
        except NoSuchElementException:
            return False

    def get_price(self):
        price_block = self.browser.find_element_by_class_name('card__cost')
        return price_block.text

    def format_price(self):
        price = self.get_price()
        return round(int(re.search("(.*) руб", price).group(1).replace(" ", "")) / 1000)

    def get_address_dict(self):
        address_dict = dict()
        address_string = self.browser.find_elements_by_class_name('card__address')[0].text
        address_dict['street'] = self.get_street(address_string)
        address_dict['house'] = self.get_house(address_string)
        return address_dict

    def get_rooms(self):
        rooms_text_line = self.browser.find_element_by_class_name('card__header').text
        rooms = re.findall('[0-9]', rooms_text_line)
        if not rooms:
            rooms = re.findall('комнату', rooms_text_line)
            if rooms:
                rooms = 0
        else:
            rooms = rooms[0]
        return int(rooms)

    def get_square_dict(self):
        square_dict = dict()
        square_list = self.browser.find_elements_by_class_name('sms-card-list__value')
        for i, sq in enumerate(square_list):
            sq_type = float(re.search("(.*) м", sq.text).group(1))
            if i == 0:
                square_dict['total_sq'] = sq_type
            elif i == 1:
                square_dict['life_sq'] = sq_type
            elif i == 2:
                square_dict['kitchen_sq'] = sq_type
        return square_dict

    def get_id(self):
        flat_id = re.search('http://homes.ngs.ru/view/([0-9]*)/$', self.flat_link).group(1)
        return int(flat_id)

    def get_detail_info_dict(self):
        detail_info_dict = dict()
        detail_info = self.browser.find_elements_by_class_name('key-value_type_with-grid')
        for i in detail_info:
            x = i.find_element_by_class_name('key-value__key')
            y = i.find_element_by_class_name('key-value__value')
            detail_info_dict[x.text] = y.text
        return detail_info_dict

    def format_detail_info_dict(self):
        detail_info_dict = self.get_detail_info_dict()
        key_to_convert_in_int = ('Этаж', 'Этажность', 'Год постройки')
        detail_info_dict = {k: int(v) if k in key_to_convert_in_int
                            else v for k, v in detail_info_dict.items()}
        return detail_info_dict

    def get_commentary(self):
        try:
            commentary_block = self.browser.find_element_by_class_name('card__comments-section')
            commentary = re.sub('^Комментарий\n', '', commentary_block.text)
            return commentary
        except NoSuchElementException:
            return None

    def get_owner_name(self):
        owner_name = self.browser.find_element_by_class_name('card__author-name').text
        return owner_name

    def get_owner_phones_list(self):
        element = self.browser.find_elements_by_id("contacts_section")[0]
        self.browser.execute_script("return arguments[0].scrollIntoView();", element)
        self.browser.execute_script("window.scrollBy(0, -250);")
        self.browser.find_elements_by_id('show_phone')[0].click()
        phone_list = self.browser.find_elements_by_class_name('card__phone')
        owner_phones = []
        for phone in phone_list:
            owner_phones.append(re.sub('\D', '', phone.text))
        return owner_phones

    def get_owner_dict(self):
        owner_dict = dict()
        owner_dict['name'] = self.get_owner_name()
        owner_dict['phones'] = self.get_owner_phones_list()
        return owner_dict

    def create_flat_dict(self):
        flat_dict = dict()
        flat_dict['id'] = self.get_id()
        flat_dict['rooms'] = self.get_rooms()
        flat_dict['square'] = self.get_square_dict()
        flat_dict['details'] = self.format_detail_info_dict()
        flat_dict['owner'] = self.get_owner_dict()
        flat_dict['commentary'] = self.get_commentary()
        flat_dict['price'] = self.format_price()
        flat_dict['address'] = self.get_address_dict()
        return flat_dict

    def close_browser(self):
        self.browser.close()

    @staticmethod
    def get_street(address_string):
        return re.search(' ул. (.*),', address_string).group(1)

    @staticmethod
    def get_house(address_string):
        return re.search(' д. (.*)', address_string).group(1)
