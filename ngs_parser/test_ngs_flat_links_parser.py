import unittest
from ngs_parser.ngs_flat_links_parser import NgsFlatLinksParser


class TestNgsFlatLinksParser(unittest.TestCase):

    def setUp(self):
        self.parser = NgsFlatLinksParser('http://homes.ngs.ru/kupit/bez_posrednikov/?'
                                         'by=_orderDate&on_page=50&order=DESC')
        self.min_number_last_page = 15
        self.max_number_last_page = 45
        self.links_per_pagination = 50

    def test_get_last_page_has_valid_range(self):
        last_page = self.parser.get_last_page()
        self.assertTrue(self.min_number_last_page < last_page < self.max_number_last_page)

    def test_get_flat_links_site_parsed_to_the_last_page(self):
        last_page = self.parser.get_last_page()
        flat_links = self.parser.get_flat_links()
        min_links_in_list = (last_page - 1) * self.links_per_pagination
        print(min_links_in_list)
        self.assertGreater(len(flat_links), min_links_in_list)


if __name__ == '__main__':
    unittest.main()
