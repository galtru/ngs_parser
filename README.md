# README #

# Quick summary

The parser grabs information from one of the most popular estate websites in Novosibirsk - homes.ngs.ru

# The example of the flat data can be found in a json file :  
https://bitbucket.org/galtru/ngs_parser/src/388364d360dab6089803b9959f122e5880fdb23a/example_flat_dict_as_json.json?at=master

# ngs_flat_links_parser.py
The parser helps a user take all links to owner ads. 
The parser automatically detects the last page of pagination and iterates through all actual and available links of owner ads on the website and saves them.
All links can be found in 'flat_links.p'.

# ngs_flat_parser.py
The parser grabs information from one particular ad.
If the ad is archived or deleted the parser skips it.
The saved data includes: id of the flat on ngs.ru, address (a street name  and a house number) number of rooms, a price, available square (total, life, kitchen), a commentary, the owner information such as a name and a list of phones, the detail information of the flat such as floors, a year of construction, type e.t.c. 