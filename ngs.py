from ngs_parser.ngs_flat_parser import NgsFlatParser
# import pickle

# flat_links=list(pickle.load(open('flat_links.p','rb')))
flat_links = ['http://homes.ngs.ru/view/220724023/']
for flat in flat_links:
    ngs = NgsFlatParser(flat)
    if ngs.check_page_exist() is True and ngs.check_page_in_archive() is False:
        flat_dict = ngs.create_flat_dict()
    else:
        print(flat)
        continue
    ngs.close_browser()
